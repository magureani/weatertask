//
//  Constants.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import Foundation

struct Constants {

  static let openWeatherApiKey = "ae3a211230ecac2ac4d80b9fc665aa9f"
  
  struct Paths {
    static let baseURL = "http://api.openweathermap.org"
    static let todayApi = "/data/2.5/weather"
    static let forecastApi = "/data/2.5/forecast"
  }
  
  struct Titles {
    static let today = "Today"
    static let forecast = "Forecast"
  }
  
}
