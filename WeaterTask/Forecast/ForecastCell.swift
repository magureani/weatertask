//
//  ForecastCell.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastCell: UITableViewCell, ReusableCell {
  
  var list: WeatherList? {
    didSet {
      tempLabel.text = String(describing: list?.main?.temp.value?.toString() ?? "-") + "°"
      timeLabel.text = Date.formattedDateString(timeSince1970: list?.dt.value ?? 0, "HH:mm")
      weatherLabel.text = list?.weather.first?.main
      
      if let icon = list?.weather.first?.icon {
        let url = URL(string: "http://openweathermap.org/img/w/\(icon).png")
        weatherImage.kf.setImage(with: url)
      }
    }
  }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  let weatherImage: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = UIColor.appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let timeLabel = UILabel(text: "-:-", font: UIFont.systemFont(ofSize: 17), textColor: .black, textAlignment: .left, numberOfLines: 0)
  let weatherLabel = UILabel(text: "-", font: UIFont.systemFont(ofSize: 17), textColor: .black, textAlignment: .left, numberOfLines: 0)
  let tempLabel = UILabel(text: "-°C", font: UIFont.boldSystemFont(ofSize: 50), textColor: .systemBlue, textAlignment: .center, numberOfLines: 0)
  
  let separatorLine = UIView(backgroundColor: UIColor(white: 0.5, alpha: 0.5))
  
  func setupUI() {
    backgroundColor = .white
    
    hstack(weatherImage.withSize(CGSize(width: 50, height: 50)),
           stack(timeLabel, weatherLabel, spacing: 8),
           tempLabel, spacing: 30, alignment: .center).padLeft(20).padRight(20)
    
    addSubview(separatorLine)
    separatorLine.anchor(nil, left: timeLabel.leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.75)
  }
  
}
