//
//  ForecastController.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit
import CoreLocation
import Kingfisher

struct Datasource {
  let date: String
  var list = [WeatherList]()
}

class ForecastController: UITableViewController {
  
  var location: CLLocation?
  var forecast: Forecast?
  var datasource = [Datasource]()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.separatorStyle = .none
    tableView.register(ForecastCell.self)
    tableView.registerHeaderFooter(ForecastHeader.self)
    setupNavBar()
    getLocation()
  }
  
  fileprivate func getLocation() {
    Locator.shared.locate { (result) in
      switch result {
      case .success(let locator):
        self.location = locator.location
        self.fetchForecast()
      case .failure(let err):
        Locator.shared.authorize()
        print(err)
      }
    }
  }
  
  fileprivate func fetchForecast() {
    if !ReachabilityManager.shared.isNetworkAvailable() {
      if let result = RealmManager.shared.getObjects(type: Forecast.self) {
        self.forecast = result.first as? Forecast
        self.sort()
        return
      }
    }
    guard let lat = location?.coordinate.latitude else { return }
    guard let lon = location?.coordinate.longitude else { return }

    let parameters = ["lat": lat, "lon": lon, "units": "metric", "appid": Constants.openWeatherApiKey] as [String : Any]
    NetworkManager.request(.forecast(parameters)) { (forecast: Forecast?, err, statusCode) in
      if err != nil {
        print(err ?? "")
        return
      }
      self.forecast = forecast
      self.sort()
    }
  }
  
  fileprivate func sort() {
    guard let forecast = self.forecast else { return }
    var datesArr = [Datasource]()
    forecast.weatherList.forEach({ (list) in
        let depDateFormatted = Date.formattedDateString(timeSince1970: list.dt.value ?? 0, "EEEE")
        if !datesArr.contains(where: {$0.date == depDateFormatted}) {
          datesArr.append(Datasource(date: depDateFormatted, list: [list]))
        } else {
          if let found = datesArr.firstIndex(where: { $0.date == depDateFormatted }) {
              datesArr[found].list.append(list)
          }
        }
    })
    self.datasource = datesArr
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
  
  fileprivate func setupNavBar() {
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationController?.navigationBar.topItem?.title = Constants.Titles.forecast
    navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
  }
}

extension ForecastController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return datasource.count
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return datasource[section].list.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: ForecastCell = tableView.dequeueReusableCell(for: indexPath)
    cell.separatorLine.isHidden = datasource[indexPath.section].list.count - 1 == indexPath.row
    cell.list = datasource[indexPath.section].list[indexPath.row]
    return cell
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header: ForecastHeader = tableView.dequeueHeaderFooter()
    header.dayLabel.text = datasource[section].date.uppercased()
    return header
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }
  
  
  
}
