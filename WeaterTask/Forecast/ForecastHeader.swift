//
//  ForecastHeader.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit
import LBTAComponents

class ForecastHeader: UITableViewHeaderFooterView, ReusableCell {
  
  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setupUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  let dayLabel = UILabel(font: UIFont.systemFont(ofSize: 15), textColor: .gray, textAlignment: .left, numberOfLines: 0)
  
  func setupUI() {
    let topSeparatorView = UIView()
    topSeparatorView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
    let bottomSeparatorView = UIView()
    topSeparatorView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
    addSubview(dayLabel)
    dayLabel.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    dayLabel.anchorCenterYToSuperview()
    
    addSubview(bottomSeparatorView)
    bottomSeparatorView.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
    addSubview(topSeparatorView)
    topSeparatorView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
  }
}
