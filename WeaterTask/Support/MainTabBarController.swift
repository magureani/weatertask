//
//  MainTabBarController.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//


import UIKit
import LBTAComponents

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViewControllers()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .default
  }
  
  func pickFirstTab() {
    selectedIndex = 0
  }
  
  func setupViewControllers() {
    let todayVC = templateController(unselectedImage: #imageLiteral(resourceName: "today.png"), selectedImage: #imageLiteral(resourceName: "today.png"), rootViewController: TodayController())
    let forecastVC = templateController(unselectedImage: #imageLiteral(resourceName: "forecast.png"), selectedImage: #imageLiteral(resourceName: "forecast.png"), rootViewController: ForecastController())
    viewControllers = [todayVC, forecastVC]
    
    guard let items = tabBar.items else { return }
    items[0].title = Constants.Titles.today
    items[1].title = Constants.Titles.forecast
  }
  
  
  fileprivate func templateController(unselectedImage: UIImage, selectedImage: UIImage, rootViewController: UIViewController = UIViewController()) -> UINavigationController {
    let navController = UINavigationController(rootViewController: rootViewController)
    navController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue], for: .selected)
    navController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
    navController.tabBarItem.image = unselectedImage.withRenderingMode(.alwaysOriginal)
    navController.tabBarItem.selectedImage = selectedImage.withRenderingMode(.alwaysTemplate)
    
    UITabBar.appearance().tintColor = UIColor.systemBlue
    return navController
  }
}



