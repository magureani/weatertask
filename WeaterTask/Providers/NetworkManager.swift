//
//  NetworkManager.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//
import Foundation
import Moya
import Alamofire
import RealmSwift


let provider = MoyaProvider<APIEndPoint>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

class NetworkManager {
  
  static func request<T: Decodable>(_ api: APIEndPoint, completion: @escaping (T?, Error?, Int?) -> ()) {
    
    provider.request(api) { (result) in
      switch result {
      case let .success(response):
        
        do {
          let results = try JSONDecoder().decode(T.self, from: response.data)
          if let object = results as? Object {
            RealmManager.shared.saveObject(object)
            completion(results, nil, response.statusCode)
            return
          }
        } catch let err {
          completion(nil, err, response.statusCode)
        }
      case let .failure(error):
        completion(nil, error, nil)
      }
    }
  }
}


private func JSONResponseDataFormatter(_ data: Data) -> Data {
  do {
    let dataAsJSON = try JSONSerialization.jsonObject(with: data)
    let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
    return prettyData
  } catch {
    return data // fallback to original data if it can't be serialized.
  }
}
