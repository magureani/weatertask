//
//  ApiEndpoint.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//


import Foundation
import Moya
import Alamofire
import RealmSwift

enum APIEndPoint {
  case today(_ parameters: Parameters)
  case forecast(_ parameters: Parameters)
}

extension APIEndPoint: TargetType {
  
  var baseURL: URL {
    guard let url = URL(string: Constants.Paths.baseURL) else { fatalError("Base URL could not be configured") }
    return url
  }
  
  var path: String {
    switch self {
    case .today:
      return Constants.Paths.todayApi
    case .forecast:
      return Constants.Paths.forecastApi
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .today, .forecast:
      return .get
    }
  }
  
  var sampleData: Data {
    return Data()
  }
  
  var task: Task {
    switch self {
    case .today(let parameters), .forecast(let parameters):
      return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }
  }
  
  var headers: [String : String]? {
    return [:]
  }
  
}
