//
//  RealmManager.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//


import Foundation
import RealmSwift

class RealmManager {
  
  static let shared = RealmManager()
  
  fileprivate(set) var defaultRealm: Realm!
  
  fileprivate var config = Realm.Configuration()
  
  fileprivate init() {
    config.schemaVersion = 1
    config.migrationBlock = { migration, oldSchemaVersion in
      
    }
    do {
      defaultRealm = try Realm(configuration: config)
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  func eraseAll() {
    do {
      let realm = try createRealm()
      try realm.write {
        realm.deleteAll()
      }
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  func createRealm() throws -> Realm {
    return try Realm(configuration: config)
  }
  
  func realmInst() -> Realm {
    return RealmManager.shared.defaultRealm
  }
  
  /** Must be called from main thread */
  func saveObject(_ object: Object, update: Bool = true)  {
    do {
      let realm = self.realmInst()
      try realm.write() {
        realm.add(object, update: update)
      }
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  /** Must be called from main thread */
  func saveObjects(_ objects: [Object], update: Bool = true)  {
    do {
      let realm = self.realmInst()
      
      try realm.write() {
        objects.forEach() { realm.add($0, update: update) }
      }
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  func getObjects(type: Object.Type) -> Results<Object>? {
    return self.realmInst().objects(type)
  }
  
  func getObject(type: Object.Type, primaryKey: String = "id") -> Object? {
    return self.realmInst().object(ofType: type, forPrimaryKey: primaryKey)
  }
}
