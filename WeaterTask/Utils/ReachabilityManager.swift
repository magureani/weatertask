//
//  ReachabilityManager.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//


import Foundation

class ReachabilityManager {
  
  static let shared = ReachabilityManager()
  var reachability : Reachability!
  var isOff = false
  
  func observeInternetReachability() {
    self.reachability = Reachability()
    NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
    do {
      try self.reachability.startNotifier()
    }
    catch(let error) {
      print("Error occured while starting reachability notifications : \(error.localizedDescription)")
    }
  }
  
  func isNetworkAvailable() -> Bool {
    return reachability.connection != .none
  }
  
  func checkInternetConnectionPlan() -> Reachability.Connection {
    return reachability.connection
  }
  
  @objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    switch reachability.connection {
    case .cellular:
      print("Network available via Cellular Data.")
      break
    case .wifi:
      break
    case .none:
      print("Network is not available.")
      break
    }
  }
}
