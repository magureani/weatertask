//
//  Extensions.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit

protocol ReusableCell: class {
  static var defaultReuseIdentifier: String { get }
}

extension ReusableCell where Self: UIView {
  static var defaultReuseIdentifier: String {
    return String(describing: self)
  }
}

extension UITableView {
  
  func registerHeaderFooter<T: UITableViewHeaderFooterView>(_: T.Type) where T: ReusableCell {
    register(T.self, forHeaderFooterViewReuseIdentifier: T.defaultReuseIdentifier)
  }
  
  func register<T: UITableViewCell>(_: T.Type) where T: ReusableCell {
    register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
  }
  
  func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T where T: ReusableCell {
    guard let cell = dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
      fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
    }
    return cell
  }
  
  func dequeueHeaderFooter<T: UITableViewHeaderFooterView>() -> T where T: ReusableCell {
    guard let headerFooterView = dequeueReusableHeaderFooterView(withIdentifier: T.defaultReuseIdentifier) as? T else {
      fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
    }
    return headerFooterView
  }
}

extension UIColor {
  
  static var systemBlue: UIColor {
    return UIButton(type: .system).tintColor
  }
  
  static var appYellow: UIColor {
    return UIColor(r: 250, g: 200, b: 50)
  }
  
  func as1ptImage() -> UIImage {
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    setFill()
    UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
    let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    UIGraphicsEndImageContext()
    return image
  }
  
}

extension Double {
  func string(maximumFractionDigits: Int = 2) -> String {
    let s = String(format: "%.\(maximumFractionDigits)f", self)
    for i in stride(from: 0, to: -maximumFractionDigits, by: -1) {
      if s[s.index(s.endIndex, offsetBy: i - 1)] != "0" {
        return String(s[..<s.index(s.endIndex, offsetBy: i)])
      }
    }
    return String(s[..<s.index(s.endIndex, offsetBy: -maximumFractionDigits - 1)])
  }
  
  func toString() -> String {
    if self >= Double(Int.min) && self < Double(Int.max) {
      return "\(Int(self))"
    } else {
      return "-"
    }
  }
}


extension Date {
  var startOfDay: Date? {
    var calendar = Calendar(identifier: .gregorian)
    calendar.timeZone = TimeZone(abbreviation: "UTC")!
    return calendar.startOfDay(for: self)
  }
  
  static func formattedDateString(timeSince1970: Double, _ format: String) -> String {    
    let timestampDate = Date(timeIntervalSince1970: Double(timeSince1970))
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current
    dateFormatter.dateFormat = format
    
    let formattedDateString = dateFormatter.string(from: timestampDate)
    return formattedDateString
  }

}
