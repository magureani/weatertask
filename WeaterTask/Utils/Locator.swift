//
//  Locator.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit
import CoreLocation

class Locator: NSObject, CLLocationManagerDelegate {
  enum Result <T> {
    case success(T)
    case failure(Error)
  }
  
  static let shared: Locator = Locator()
  
  typealias Callback = (Result <Locator>) -> Void
  
  var requests: Array <Callback> = Array <Callback>()
  
  var location: CLLocation? { return sharedLocationManager.location  }
  
  lazy var sharedLocationManager: CLLocationManager = {
    let newLocationmanager = CLLocationManager()
    newLocationmanager.delegate = self
    return newLocationmanager
  }()
  
  // MARK: - Authorization
  
  class func authorize() { shared.authorize() }
  func authorize() { sharedLocationManager.requestWhenInUseAuthorization() }
  
  // MARK: - Helpers
  func locate(callback: @escaping Callback) {
    self.requests.append(callback)
    sharedLocationManager.startUpdatingLocation()
  }
  
  func reset() {
    self.requests = Array <Callback>()
    sharedLocationManager.stopUpdatingLocation()
  }
  
  // MARK: - Delegate
  private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
    for request in self.requests { request(.failure(error)) }
    self.reset()
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: Array <CLLocation>) {
    for request in self.requests { request(.success(self)) }
    self.reset()
  }
  
}
