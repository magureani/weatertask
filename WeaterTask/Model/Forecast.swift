//
//  Forecast.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Forecast: Object, Decodable {
  let weatherList = List<WeatherList>()
  @objc dynamic var city: City?
  @objc dynamic var primaryKey: String?
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  enum CodingKeys: String, CodingKey {
    case weatherList = "list"
    case city
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.city = try container.decodeIfPresent(City.self, forKey: .city)
    let weatherArr = try container.decodeIfPresent([WeatherList].self, forKey: .weatherList) ?? [WeatherList()]
    weatherList.append(objectsIn: weatherArr)
    primaryKey = "autoUpdatedForecast"
  }
}

// MARK: - List
class WeatherList: Object, Decodable {
  @objc dynamic var primaryKey: String?

  let dt = RealmOptional<Double>()
  @objc dynamic var main: Main?
  let weather = List<Weather>()
  @objc dynamic var wind: Wind?

  enum CodingKeys: String, CodingKey {
    case dt, main, weather, wind
  }
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.dt.value = try container.decodeIfPresent(Double.self, forKey: .dt)
    self.main = try container.decodeIfPresent(Main.self, forKey: .main)
    self.wind = try container.decodeIfPresent(Wind.self, forKey: .wind)
    
    let weatherArr = try container.decodeIfPresent([Weather].self, forKey: .weather) ?? [Weather()]
    weather.append(objectsIn: weatherArr)
    
    primaryKey = String(describing: dt.value)
    main?.primaryKey = String(describing: dt.value)
  }
}

// MARK: - City
class City: Object, Decodable {
  let id = RealmOptional<Int>()
  @objc dynamic var name: String?
  @objc dynamic var primaryKey: String?
  
  enum CodingKeys: String, CodingKey {
    case id, name
  }
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.id.value = try container.decodeIfPresent(Int.self, forKey: .id)
    self.name = try container.decodeIfPresent(String.self, forKey: .name)
    primaryKey = String(describing: id.value)
  }
}

class Rain: Object, Decodable {
  let the3H = RealmOptional<Double>()
  @objc dynamic var primaryKey: String?
  
  enum CodingKeys: String, CodingKey {
    case the3H = "3h"
  }
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.the3H.value = try container.decodeIfPresent(Double.self, forKey: .the3H)
  }
}

