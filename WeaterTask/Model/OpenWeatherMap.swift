//
//  OpenWeatherMap.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

// MARK: - OpenWeatherMap
class OpenWeatherMap: Object, Decodable {
  @objc dynamic var main: Main?
  @objc dynamic var wind: Wind?
  @objc dynamic var rain: Rain?
  let dt = RealmOptional<Double>()
  let id = RealmOptional<Int>()
  @objc dynamic var name: String?
  
  let weather = List<Weather>()
  
  @objc dynamic var primaryKey: String?
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  enum CodingKeys: String, CodingKey {
    case main, wind, rain, dt, id, name, weather
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.id.value = try container.decodeIfPresent(Int.self, forKey: .id)
    self.main = try container.decodeIfPresent(Main.self, forKey: .main)
    self.wind = try container.decodeIfPresent(Wind.self, forKey: .wind)
    self.rain = try container.decodeIfPresent(Rain.self, forKey: .rain)
    self.dt.value = try container.decodeIfPresent(Double.self, forKey: .dt)
    self.name = try container.decodeIfPresent(String.self, forKey: .name)
    let weatherList = try container.decodeIfPresent([Weather].self, forKey: .weather) ?? [Weather()]
    weather.append(objectsIn: weatherList)
    primaryKey = String(describing: "autoUpdatedModel")
  }
}



// MARK: - Main
class Main: Object, Decodable {
  let temp = RealmOptional<Double>()
  let pressure = RealmOptional<Double>()
  let humidity = RealmOptional<Double>()
  @objc dynamic var primaryKey: String?

  enum CodingKeys: String, CodingKey {
    case temp, pressure, humidity
  }
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.temp.value = try container.decodeIfPresent(Double.self, forKey: .temp)
    self.pressure.value = try container.decodeIfPresent(Double.self, forKey: .pressure)
    self.humidity.value = try container.decodeIfPresent(Double.self, forKey: .humidity)
  }
}

// MARK: - Weather
class Weather: Object, Decodable {
  let id = RealmOptional<Int>()
  @objc dynamic var main: String?
  @objc dynamic var icon: String?
  
  enum CodingKeys: String, CodingKey {
    case id, main, icon
  }
  
  @objc dynamic var primaryKey: String?
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.id.value = try container.decodeIfPresent(Int.self, forKey: .id)
    self.main = try container.decodeIfPresent(String.self, forKey: .main)
    self.icon = try container.decodeIfPresent(String.self, forKey: .icon)
    primaryKey = String(describing: id.value)
  }
  
}

// MARK: - Wind
class Wind: Object, Decodable {
  let speed = RealmOptional<Double>()
  
  enum CodingKeys: String, CodingKey {
    case speed
  }
  
  @objc dynamic var primaryKey: String?
  
  override static func primaryKey() -> String? {
    return "primaryKey"
  }
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.speed.value = try container.decodeIfPresent(Double.self, forKey: .speed)
  }
}
