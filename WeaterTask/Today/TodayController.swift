//
//  HomeController.swift
//  WeaterTask
//
//  Created by Igor on 5/28/19.
//  Copyright © 2019 Igor. All rights reserved.
//

import UIKit
import CoreLocation
import LBTATools
import FirebaseDatabase

class TodayController: UIViewController {
  
  var location: CLLocation?
  var openWeather: OpenWeatherMap?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    Locator.shared.authorize()
    setupUI()
    setupNavBar()
    getLocation()
  }
  
  fileprivate func getLocation() {
    Locator.shared.locate { (result) in
      switch result {
      case .success(let locator):
        self.location = locator.location
        self.fetchTodaysWeather()
      case .failure(let err):
        Locator.shared.authorize()
        print(err)
      }
    }
  }
  
  fileprivate func fetchTodaysWeather() {
    if !ReachabilityManager.shared.isNetworkAvailable() {
      if let result = RealmManager.shared.getObjects(type: OpenWeatherMap.self) {
        setText(with: result.first as? OpenWeatherMap)
        return
      }
    }
    guard let lat = location?.coordinate.latitude else { return }
    guard let lon = location?.coordinate.longitude else { return }
    let parameters = ["lat": lat, "lon": lon, "units": "metric", "appid": Constants.openWeatherApiKey] as [String : Any]
    NetworkManager.request(.today(parameters)) { (openWeatherMap: OpenWeatherMap?, err, statusCode) in
      if err != nil {
        let alertController = UIAlertController(title: "Error", message: "Failed to fetch weather information", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        return
      }
      self.openWeather = openWeatherMap
      self.setText(with: openWeatherMap)
      self.writeToFirebase()
    }
  }

  fileprivate func writeToFirebase() {
    let ref = Database.database().reference()
    guard let name = openWeather?.name else { return }
    guard let temperature = openWeather?.main?.temp.value else { return }
    guard let lat = location?.coordinate.latitude else { return }
    guard let lon = location?.coordinate.longitude else { return }
    guard let date = openWeather?.dt.value else { return }
    let formattedDate = Date.formattedDateString(timeSince1970: date, "dd.MMM.YYYY hh:mm")

    ref.child("weather").child(date.string()).childByAutoId().setValue(["city": name,
                                   "temperature": temperature,
                                   "lastUpdate": Date().timeIntervalSince1970,
                                   "lat": lat,
                                   "long": lon,
                                   "date": formattedDate])
  }

  fileprivate func setText(with openWeather: OpenWeatherMap?) {
    guard let openWeather = openWeather else { return }
    DispatchQueue.main.async {
      self.cityLabel.text = openWeather.name
      self.tempLabel.text = String(describing: openWeather.main?.temp.value?.toString() ?? "-") + "°C | \(openWeather.weather.first?.main ?? "-")"
      self.humidityLabel.text = String(describing: openWeather.main?.humidity.value?.toString() ?? "-") + " %"
      self.rainLabel.text = String(describing: openWeather.rain?.the3H.value ?? 0) + " mm"
      self.pressureLabel.text = String(describing: openWeather.main?.pressure.value?.toString() ?? "-") + " hPa"
      self.windLabel.text = String(describing: openWeather.wind?.speed.value?.string(maximumFractionDigits: 1) ?? "-") + " m/s"
    }
  }
  
  fileprivate func setupNavBar() {
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationController?.navigationBar.topItem?.title = Constants.Titles.today
    navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(handleRefresh))
    self.navigationItem.rightBarButtonItem?.tintColor = .systemBlue
  }
  
  @objc fileprivate func handleRefresh() {
    getLocation()
  }
  
  @objc fileprivate func handleShare() {
    if let temperature = openWeather?.main?.temp.value, let city = openWeather?.name {
      let text = "Temperature in " + city + "is" + temperature.toString()
      let textShare = [text]
      let activityViewController = UIActivityViewController(activityItems: textShare, applicationActivities: nil)
      activityViewController.popoverPresentationController?.sourceView = self.view
      self.present(activityViewController, animated: true, completion: nil)
    }
  }
  
  let tempImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let humidityImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let rainImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let pressureImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let windImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let compasImageView: UIImageView = {
    let iv = UIImageView()
    iv.image = #imageLiteral(resourceName: "today.png").withRenderingMode(.alwaysTemplate)
    iv.tintColor = .appYellow
    iv.contentMode = .scaleAspectFit
    return iv
  }()
  
  let cityLabel = UILabel(text: "-", font: UIFont.systemFont(ofSize: 17), textColor: .gray, textAlignment: .center)
  let tempLabel = UILabel(text: "-", font: UIFont.systemFont(ofSize: 28), textColor: .systemBlue, textAlignment: .center)
  let humidityLabel = UILabel(text: "- %", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center)
  let rainLabel = UILabel(text: "- mm", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center)
  let pressureLabel = UILabel(text: "- hPa", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center)
  let windLabel = UILabel(text: "- km/h", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center)
  let compasLabel = UILabel(text: "SE", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center)
  let shareButton = UIButton(title: "Share", titleColor: .red, font: UIFont.boldSystemFont(ofSize: 18), backgroundColor: .clear, target: self, action: #selector(handleShare))
  fileprivate func setupUI() {
    let separator = UIView()
    separator.backgroundColor = .lightGray
    
    let bottomSeparator = UIView()
    bottomSeparator.backgroundColor = .lightGray
    
    view.stack(UIView(),
               tempImageView.withHeight(80),
               cityLabel,
               tempLabel,
               view.hstack(separator.withHeight(1)).padTop(20).padLeft(100).padRight(100),
               view.hstack(humidityImageView, rainImageView, pressureImageView, distribution: .fillEqually).padTop(20).withHeight(50),
               view.hstack(humidityLabel, rainLabel, pressureLabel, distribution: .fillEqually),
               view.hstack(windImageView, compasImageView, distribution: .fillEqually).withMargins(.init(top: 20, left: 40, bottom: 0, right: 40)).withHeight(50),
               view.hstack(windLabel, compasLabel, distribution: .fillEqually).withMargins(.init(top: 0, left: 40, bottom: 0, right: 40)),
               view.hstack(bottomSeparator.withHeight(1)).padTop(20).padLeft(100).padRight(100),
               view.stack(shareButton).padTop(20),
               UIView(),
                spacing: 12).padTop(50)
  }
}
